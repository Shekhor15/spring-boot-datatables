package com.datatablefetchsample.utils;

import com.datatablefetchsample.model.DataTablesResponse;
import com.datatablefetchsample.model.response.Pagination;
import com.datatablefetchsample.model.response.ServiceResponse;

public abstract class Converter {
    public static ServiceResponse pageToServiceResponse() {


        return new ServiceResponse(null, null, new Pagination(0L,0,0,0));
    }

    public static DataTablesResponse getDatatableResponseFromServiceResponse(ServiceResponse serviceResponse) {
        return new DataTablesResponse(0,serviceResponse.getPagination().getTotalCount(),serviceResponse.getPagination().getSize(),null,null);
    }
}
