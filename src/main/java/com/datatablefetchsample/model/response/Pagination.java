package com.datatablefetchsample.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data

public class Pagination implements Serializable{

    private Long totalCount;

    private Integer currentPageSize;

    private Integer page;

    private Integer size;

    public Pagination(Long totalCount, Integer currentPageSize, Integer page, Integer size) {
        this.totalCount = totalCount;
        this.currentPageSize = currentPageSize;
        this.page = page;
        this.size = size;
    }
}