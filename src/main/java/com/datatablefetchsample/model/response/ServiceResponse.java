package com.datatablefetchsample.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
public class ServiceResponse <T> implements Serializable{

    private Metadata meta;

    private T data;

    private Pagination pagination;

    public ServiceResponse(Metadata meta, T data,Pagination pagination) {
        this.meta = meta;
        this.data = data;
        this.pagination = pagination;
    }

    public ServiceResponse() {
    }
}