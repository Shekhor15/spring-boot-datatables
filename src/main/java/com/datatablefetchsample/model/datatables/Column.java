package com.datatablefetchsample.model.datatables;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Column {

  private String data;

  private String name;

  private Boolean searchable;

  private Boolean orderable;

  private Search search;

}