package com.datatablefetchsample.model.datatables;

import com.datatablefetchsample.model.datatables.Column;
import com.datatablefetchsample.model.datatables.Order;
import com.datatablefetchsample.model.datatables.Search;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataTablesInput implements Serializable {

    private Integer draw;

    private Integer start;

    private Integer length;

    private Search search;
    private List<Order> order;

    private List<Column> columns;

}