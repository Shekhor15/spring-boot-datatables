package com.datatablefetchsample.model;

import lombok.Data;
import java.awt.peer.ListPeer;
import java.util.Collections;
import java.util.List;

@Data
public class DataTablesResponse <T>{
    private int draw;
    private long recordsTotal;
    private long recordsFiltered;
    private String error;
    private T data;

    public DataTablesResponse(int draw, long recordsTotal, long recordsFiltered, String error, T data) {
        this.draw = draw;
        this.recordsTotal = recordsTotal;
        this.recordsFiltered = recordsFiltered;
        this.error = error;
        this.data = data;
    }
}
