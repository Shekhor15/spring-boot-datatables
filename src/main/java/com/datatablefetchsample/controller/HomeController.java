package com.datatablefetchsample.controller;


import com.datatablefetchsample.model.DataTablesResponse;
import com.datatablefetchsample.model.datatables.DataTablesInput;
import com.datatablefetchsample.model.response.ServiceResponse;
import com.datatablefetchsample.utils.Converter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.xml.bind.Element;

@Controller
public class HomeController {
    @GetMapping("home")
    public ModelAndView getMethod(){

       return new ModelAndView("welcome");
    }
    @PostMapping("result")
    @ResponseBody
    public DataTablesResponse<Element> list(@RequestBody final DataTablesInput dataTablesRequest) {

        return Converter.getDatatableResponseFromServiceResponse(new ServiceResponse());


    }
    
}
