<!DOCTYPE html>
<html>
<head>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
    <script>
        $(document).ready(function() {
            $('#example').DataTable().destroy();
            $('#example').DataTable( {
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "result",
                    "type": "POST",
                    "dataType": "json",
                    "contentType": 'application/json; charset=utf-8',
                    data: function (d) {
                        return JSON.stringify(d)
                    }
                },
                "columns": [
                    { "data": "data" },
                    { "data": "name" },
                    { "data": "searchable" },
                    { "data": "orderable" },
                    { "data": "search" }

                ]
            } );
        } );
    </script>
</head>
<body>

<table id="example" class="table display" style="width:100%">
    <thead>
    <tr>
        <th>data</th>
        <th>name</th>
        <th>searchable</th>
        <th>orderable</th>
        <th>search</th>

    </tr>
    </thead>

</table>

</body>
</html>
